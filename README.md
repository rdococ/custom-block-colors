# Custom Block Colors

A userscript for Scratch 3.0. Make your custom blocks more colorful!

## How do I install it?

You can use any plugin or extension (e.g. [Greasemonkey](https://www.greasespot.net/), [Tampermonkey](https://tampermonkey.net/)) that allows you to install userscripts. I use Tampermonkey.

Once you've installed either of those, you can install the userscript by going [here](https://gitlab.com/rdococ/custom-block-colors/raw/master/CustomBlockColors.user.js).

## How do I use it?

Simply add a small text tag anywhere in the custom block, like "[mo]" or "[li]". The tag should be enclosed by square brackets, and have the first two letters of the category whose color you want to copy. For example, "[mo]" makes the custom block look like a Motion block, and "[li]" makes the custom block look like a List block. All categories are supported, except for "My Blocks" and any extensions.

Currently, there is no support for themes that change the colors of the blocks. Colored custom blocks will have the color they would usually have without the theme. If your theme makes Motion blocks red, then a custom Motion block will still look blue.