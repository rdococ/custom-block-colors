// ==UserScript==
// @name			Custom Block Colors
// @namespace		https://gitlab.com/rdococ
// @version			0.3
// @description		Give your custom blocks some color!
// @author			rdococ
// @match			https://scratch.mit.edu/projects/*
// @grant			none
// ==/UserScript==

// Credit to StackOverflow and Erik Vold
// https://stackoverflow.com/questions/2246901/how-can-i-use-jquery-in-greasemonkey-scripts-in-google-chrome
// https://web.archive.org/web/20130804120117/http://erikvold.com/blog/index.cfm/2010/6/14/using-jquery-with-a-user-script
function addJQuery(callback) {
	var script = document.createElement("script");
	script.setAttribute("src", "//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js");
	script.addEventListener('load', function() {
		var script = document.createElement("script");
		script.textContent = "window.jQ=jQuery.noConflict(true);(" + callback.toString() + ")();";
		document.body.appendChild(script);
	}, false);
	document.body.appendChild(script);
}

function main() {
	// These are all of the colors for each category.
	var colors = {
		motion: {
			stroke: '#3373CC',
			fill: '#4C97FF'
		},
		variables: {
			stroke: '#DB6E00',
			fill: '#FF8C1A'
		},
		list: {
			stroke: '#E64D00',
			fill: '#FF661A'
		},
		looks: {
			stroke: 'rgb(119, 77, 203)',
			fill: 'rgb(153, 102, 255)'
		},
		events: {
			stroke: 'rgb(204, 153, 0)',
			fill: 'rgb(255, 191, 0)'
		},
		control: {
			stroke: 'rgb(207, 139, 23)',
			fill: 'rgb(255, 171, 25)'
		},
		sensing: {
			stroke: 'rgb(46, 142, 184)',
			fill: 'rgb(92, 177, 214)'
		},
		operators: {
			stroke: 'rgb(56, 148, 56)',
			fill: 'rgb(89, 192, 89)'
		}
	};
	
	// The best way that I could find to do stuff when elements were created is using MutationObservers.
	// This implementation is fairly inefficient, because it doesn't use the "mutations" table that includes the newly created elements.
	// I'm trying to fix that, but the fix is currently quite inconsistent and I'm not sure why.
	var observer = new WebKitMutationObserver(function(mutations) {
		// We handle blocks and boolean inputs together, and text inputs separately.
		// This is because boolean inputs don't need to be recolored when a block is dropped into them and taken out.
		// On the other hand, text inputs do.
		var allBlocks = jQ(".blocklyDraggable:not([data-category]):not([data-shapes~=hat])")
		var allTextInputs = jQ("[data-argument-type=text]")
		
		Object.keys(colors).forEach(function(key) {
			var blocks = allBlocks.children().filter(".blocklyText:contains(\"[" + key.substring(0, 2) + "]\")").parent();
			
			if (blocks.length > 0) {
				blocks.addClass("alreadyColored");

				blocks.children(".blocklyPath.blocklyBlockBackground")
					.attr("stroke", colors[key]["stroke"])
					.attr("fill", colors[key]["fill"]);
				
				blocks.children("[data-argument-type=boolean]")
					.attr("stroke", colors[key]["stroke"])
					.attr("fill", colors[key]["stroke"]);
				
				// Not setting a timeout here seems to cause a crash.
				window.setTimeout(function () {
					blocks.children(".blocklyText")
						.each(function () {
							this.innerHTML = this.innerHTML.replace("[" + key.substring(0, 2) + "]", "");
						});
				}, 100);
			}
			
			// Find text inputs whose parents are definitely custom blocks, but don't have the usual custom block color.
			// Then set their stroke to the custom block's stroke.
			// This shouldn't break any block recoloring userstyles unless their text input outlines are different from their block outlines,
			// but the rest of this script does so anyway.
			var textInputs = allTextInputs.parent()
				.filter(".blocklyDraggable:not([data-category]):has(.blocklyPath.blocklyBlockBackground:not([fill=#FF6680]))")
				.children("[data-argument-type=text]")
			
			if (textInputs.length > 0) {
				textInputs.each(function () {
					jQ(this).children(".blocklyPath").attr("stroke",
						jQ(this).parent().children(".blocklyPath.blocklyBlockBackground").attr("stroke"));
				});
			}
		})
	});
	observer.observe(document, {
		childList: true,
		subtree: true
	});
}

addJQuery(main);